<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0b54fc22-b2be-46c5-8a18-178aa51a0d26(RefuelingLanguage.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="df832bff-ff62-4075-b8a9-22a469b64567" name="RefuelingLanguage" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="df832bff-ff62-4075-b8a9-22a469b64567" name="RefuelingLanguage">
      <concept id="4030636028377905068" name="RefuelingLanguage.structure.Requsites" flags="ng" index="3_H8mK">
        <property id="4030636028377905077" name="bankAccountNumber" index="3_H8mD" />
        <property id="4030636028377905069" name="INN" index="3_H8mL" />
        <property id="4030636028377905070" name="BIC" index="3_H8mM" />
      </concept>
      <concept id="8157872828699347865" name="RefuelingLanguage.structure.Contract" flags="ng" index="3WmhQ4">
        <property id="4030636028378055418" name="agreedDateTime" index="3_Gz3A" />
        <property id="4030636028378055452" name="comments" index="3_Gz40" />
        <property id="8157872828699347868" name="number" index="3WmhQ1" />
        <property id="8157872828699347875" name="creationDateTime" index="3WmhQY" />
        <child id="8157872828699347958" name="airports" index="3WmhRF" />
        <child id="8157872828699347953" name="supplier" index="3WmhRG" />
        <child id="8157872828699347955" name="customer" index="3WmhRI" />
      </concept>
      <concept id="8157872828699347886" name="RefuelingLanguage.structure.Account" flags="ng" index="3WmhQN">
        <property id="4030636028377905086" name="name" index="3_H8my" />
        <property id="8157872828699347887" name="IATA" index="3WmhQM" />
        <child id="4030636028377905084" name="requisites" index="3_H8mw" />
      </concept>
      <concept id="8157872828699347880" name="RefuelingLanguage.structure.Airport" flags="ng" index="3WmhQP">
        <property id="4030636028378055443" name="IATA" index="3_Gz4f" />
        <property id="8157872828699347881" name="name" index="3WmhQO" />
      </concept>
    </language>
  </registry>
  <node concept="3WmhQ4" id="3vJGxNinA8U">
    <property role="TrG5h" value="test" />
    <property role="3WmhQ1" value="1" />
    <property role="3WmhQY" value="12:56 03/09/2020" />
    <property role="3_Gz3A" value="-" />
    <property role="3_Gz40" value="-" />
    <node concept="3WmhQN" id="3vJGxNinA8V" role="3WmhRG">
      <property role="3_H8my" value="Gazprom" />
      <property role="3WmhQM" value="LED" />
      <node concept="3_H8mK" id="3vJGxNinA8W" role="3_H8mw">
        <property role="3_H8mD" value="8768374378534875" />
        <property role="3_H8mL" value="4875234875" />
        <property role="3_H8mM" value="35456456" />
      </node>
    </node>
    <node concept="3WmhQN" id="3vJGxNinA8X" role="3WmhRI">
      <property role="3WmhQM" value="MOW" />
      <property role="3_H8my" value="Aeroflot" />
      <node concept="3_H8mK" id="3vJGxNinA8Y" role="3_H8mw">
        <property role="3_H8mD" value="847568475834756" />
        <property role="3_H8mL" value="547457547" />
        <property role="3_H8mM" value="36354" />
      </node>
    </node>
    <node concept="3WmhQP" id="3vJGxNinA8Z" role="3WmhRF">
      <property role="3WmhQO" value="Zhukovo" />
      <property role="3_Gz4f" value="MOW" />
    </node>
  </node>
</model>

