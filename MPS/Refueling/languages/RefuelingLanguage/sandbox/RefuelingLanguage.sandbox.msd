<?xml version="1.0" encoding="UTF-8"?>
<solution name="RefuelingLanguage.sandbox" uuid="cd6ba130-4a60-4f03-a9ad-b329dfbbaf58" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <languageVersions>
    <language slang="l:df832bff-ff62-4075-b8a9-22a469b64567:RefuelingLanguage" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
  </languageVersions>
  <dependencyVersions>
    <module reference="cd6ba130-4a60-4f03-a9ad-b329dfbbaf58(RefuelingLanguage.sandbox)" version="0" />
  </dependencyVersions>
</solution>

