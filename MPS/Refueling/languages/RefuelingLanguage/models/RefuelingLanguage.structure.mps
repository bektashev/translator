<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:2e0b8296-f8e9-4961-a37e-113c0dcf8c13(RefuelingLanguage.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="3348158742936976480" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ng" index="25R33">
        <property id="1421157252384165432" name="memberId" index="3tVfz5" />
        <property id="672037151186491528" name="presentation" index="1L1pqM" />
      </concept>
      <concept id="3348158742936976479" name="jetbrains.mps.lang.structure.structure.EnumerationDeclaration" flags="ng" index="25R3W">
        <reference id="1075010451642646892" name="defaultMember" index="1H5jkz" />
        <child id="3348158742936976577" name="members" index="25R1y" />
      </concept>
      <concept id="1082978164218" name="jetbrains.mps.lang.structure.structure.DataTypeDeclaration" flags="ng" index="AxPO6">
        <property id="7791109065626895363" name="datatypeId" index="3F6X1D" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="4TC5SpZFOHT">
    <property role="EcuMT" value="5649791598971538297" />
    <property role="TrG5h" value="PriceNotice" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="74Q_xQds5yC" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699400360" />
      <property role="TrG5h" value="negotiation" />
      <ref role="AX2Wp" node="7UJp4hhe3vD" resolve="Negotiation" />
    </node>
    <node concept="1TJgyi" id="74Q_xQds5yG" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699400364" />
      <property role="TrG5h" value="number" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="74Q_xQds5yL" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699400369" />
      <property role="TrG5h" value="creationDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAKR" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777721911" />
      <property role="TrG5h" value="startDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="4TC5SpZFOIn" role="1TKVEl">
      <property role="IQ2nx" value="5649791598971538327" />
      <property role="TrG5h" value="price" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="4TC5SpZFOIs" role="1TKVEl">
      <property role="IQ2nx" value="5649791598971538332" />
      <property role="TrG5h" value="period" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOALC" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777721960" />
      <property role="TrG5h" value="NDS" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="74Q_xQds5yT" role="1TKVEi">
      <property role="IQ2ns" value="8157872828699400377" />
      <property role="20kJfa" value="contract" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSIp" resolve="Contract" />
    </node>
    <node concept="1TJgyj" id="7AdOa6oOAKK" role="1TKVEi">
      <property role="IQ2ns" value="8758886242777721904" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="supplier" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSII" resolve="Account" />
    </node>
    <node concept="1TJgyj" id="7AdOa6oOAKL" role="1TKVEi">
      <property role="IQ2ns" value="8758886242777721905" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="customer" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSII" resolve="Account" />
    </node>
    <node concept="1TJgyj" id="7AdOa6oOAKM" role="1TKVEi">
      <property role="IQ2ns" value="8758886242777721906" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="airport" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSIC" resolve="Airport" />
    </node>
    <node concept="1TJgyj" id="7AdOa6oOANm" role="1TKVEi">
      <property role="IQ2ns" value="8758886242777722070" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="product" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="7AdOa6oOAMQ" resolve="Gas" />
    </node>
    <node concept="PrWs8" id="74Q_xQds6hL" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="25R3W" id="7UJp4hhe3vD">
    <property role="3F6X1D" value="9128625214286477289" />
    <property role="TrG5h" value="Negotiation" />
    <ref role="1H5jkz" node="7UJp4hhe3vF" resolve="waitingSupplier" />
    <node concept="25R33" id="7UJp4hhe3vE" role="25R1y">
      <property role="3tVfz5" value="9128625214286477290" />
      <property role="TrG5h" value="waitingCustomer" />
      <property role="1L1pqM" value="Waiting Customer" />
    </node>
    <node concept="25R33" id="7UJp4hhe3vF" role="25R1y">
      <property role="3tVfz5" value="9128625214286477291" />
      <property role="TrG5h" value="waitingSupplier" />
      <property role="1L1pqM" value="Waiting Supplier" />
    </node>
    <node concept="25R33" id="7UJp4hhe3vI" role="25R1y">
      <property role="3tVfz5" value="9128625214286477294" />
      <property role="TrG5h" value="approved" />
      <property role="1L1pqM" value="Approved" />
    </node>
    <node concept="25R33" id="7UJp4hhe3vM" role="25R1y">
      <property role="3tVfz5" value="9128625214286477298" />
      <property role="TrG5h" value="rejected" />
      <property role="1L1pqM" value="Rejected" />
    </node>
  </node>
  <node concept="25R3W" id="7UJp4hhe3w6">
    <property role="3F6X1D" value="9128625214286477318" />
    <property role="TrG5h" value="PaymentType" />
    <ref role="1H5jkz" node="7UJp4hhe3w7" resolve="prePayment" />
    <node concept="25R33" id="7UJp4hhe3w7" role="25R1y">
      <property role="3tVfz5" value="9128625214286477319" />
      <property role="TrG5h" value="prePayment" />
      <property role="1L1pqM" value="Prepayment" />
    </node>
    <node concept="25R33" id="7UJp4hhe3w8" role="25R1y">
      <property role="3tVfz5" value="9128625214286477320" />
      <property role="TrG5h" value="postPayment" />
      <property role="1L1pqM" value="Postpayment" />
    </node>
    <node concept="25R33" id="7UJp4hhe3wb" role="25R1y">
      <property role="3tVfz5" value="9128625214286477323" />
      <property role="TrG5h" value="defferedPayment" />
      <property role="1L1pqM" value="Deffered Payment" />
    </node>
  </node>
  <node concept="1TIwiD" id="74Q_xQdrSIp">
    <property role="EcuMT" value="8157872828699347865" />
    <property role="TrG5h" value="Contract" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="74Q_xQds4OP" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699397429" />
      <property role="TrG5h" value="negotiation" />
      <ref role="AX2Wp" node="7UJp4hhe3vD" resolve="Negotiation" />
    </node>
    <node concept="1TJgyi" id="74Q_xQdrSIs" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699347868" />
      <property role="TrG5h" value="number" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="74Q_xQdrSIz" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699347875" />
      <property role="TrG5h" value="creationDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="74Q_xQdrSJZ" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699347967" />
      <property role="TrG5h" value="paymentType" />
      <ref role="AX2Wp" node="7UJp4hhe3w6" resolve="PaymentType" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinobU" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378055418" />
      <property role="TrG5h" value="agreedDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinocs" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378055452" />
      <property role="TrG5h" value="comments" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="74Q_xQdrSJL" role="1TKVEi">
      <property role="IQ2ns" value="8157872828699347953" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="supplier" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSII" resolve="Account" />
    </node>
    <node concept="1TJgyj" id="74Q_xQdrSJN" role="1TKVEi">
      <property role="IQ2ns" value="8157872828699347955" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="customer" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSII" resolve="Account" />
    </node>
    <node concept="1TJgyj" id="74Q_xQdrSJQ" role="1TKVEi">
      <property role="IQ2ns" value="8157872828699347958" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="airports" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="74Q_xQdrSIC" resolve="Airport" />
    </node>
    <node concept="PrWs8" id="74Q_xQdrSKt" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="74Q_xQdrSIC">
    <property role="EcuMT" value="8157872828699347880" />
    <property role="TrG5h" value="Airport" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="3vJGxNinocj" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378055443" />
      <property role="TrG5h" value="IATA" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="74Q_xQdrSID" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699347881" />
      <property role="TrG5h" value="name" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="74Q_xQdrSII">
    <property role="EcuMT" value="8157872828699347886" />
    <property role="TrG5h" value="Account" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="74Q_xQdrSIJ" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699347887" />
      <property role="TrG5h" value="IATA" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNimNuY" role="1TKVEl">
      <property role="IQ2nx" value="4030636028377905086" />
      <property role="TrG5h" value="name" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="3vJGxNimNuW" role="1TKVEi">
      <property role="IQ2ns" value="4030636028377905084" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="requisites" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="3vJGxNimNuG" resolve="Requsites" />
    </node>
  </node>
  <node concept="1TIwiD" id="74Q_xQds9BM">
    <property role="EcuMT" value="8157872828699417074" />
    <property role="TrG5h" value="Request" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7AdOa6oOAKt" role="1TKVEi">
      <property role="IQ2ns" value="8758886242777721885" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="supplier" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSII" resolve="Account" />
    </node>
    <node concept="1TJgyj" id="7AdOa6oOAKu" role="1TKVEi">
      <property role="IQ2ns" value="8758886242777721886" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="customer" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSII" resolve="Account" />
    </node>
    <node concept="1TJgyj" id="7AdOa6oOAKv" role="1TKVEi">
      <property role="IQ2ns" value="8758886242777721887" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="airports" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSIC" resolve="Airport" />
    </node>
    <node concept="PrWs8" id="74Q_xQds9BN" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="3vJGxNin_9x" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378108513" />
      <property role="TrG5h" value="negotiation" />
      <ref role="AX2Wp" node="7UJp4hhe3vD" resolve="Negotiation" />
    </node>
    <node concept="1TJgyi" id="3vJGxNin_9A" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378108518" />
      <property role="TrG5h" value="creationDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNin_9G" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378108524" />
      <property role="TrG5h" value="agreedDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNin_9N" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378108531" />
      <property role="TrG5h" value="number" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinA9f" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112591" />
      <property role="TrG5h" value="period" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinA9o" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112600" />
      <property role="TrG5h" value="plannedGas" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinA9y" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112610" />
      <property role="TrG5h" value="comments" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="74Q_xQds9Cn" role="1TKVEi">
      <property role="IQ2ns" value="8157872828699417111" />
      <property role="20kJfa" value="contract" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSIp" resolve="Contract" />
    </node>
  </node>
  <node concept="1TIwiD" id="3vJGxNimNuG">
    <property role="EcuMT" value="4030636028377905068" />
    <property role="TrG5h" value="Requsites" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="3vJGxNimNuP" role="1TKVEl">
      <property role="IQ2nx" value="4030636028377905077" />
      <property role="TrG5h" value="bankAccountNumber" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNimNuH" role="1TKVEl">
      <property role="IQ2nx" value="4030636028377905069" />
      <property role="TrG5h" value="INN" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNimNuI" role="1TKVEl">
      <property role="IQ2nx" value="4030636028377905070" />
      <property role="TrG5h" value="BIC" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="3vJGxNinA9H">
    <property role="EcuMT" value="4030636028378112621" />
    <property role="TrG5h" value="Flight" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7AdOa6oOALP" role="1TKVEi">
      <property role="IQ2ns" value="8758886242777721973" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="customer" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSII" resolve="Account" />
    </node>
    <node concept="1TJgyj" id="74Q_xQds9BR" role="1TKVEi">
      <property role="IQ2ns" value="8157872828699417079" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="source" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSIC" resolve="Airport" />
    </node>
    <node concept="1TJgyj" id="74Q_xQds9BT" role="1TKVEi">
      <property role="IQ2ns" value="8157872828699417081" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="destination" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSIC" resolve="Airport" />
    </node>
    <node concept="1TJgyi" id="74Q_xQds9BP" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699417077" />
      <property role="TrG5h" value="internationalFlight" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="74Q_xQds9C0" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699417088" />
      <property role="TrG5h" value="flightNumber" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinAa8" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112648" />
      <property role="TrG5h" value="creationDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="74Q_xQds9C3" role="1TKVEl">
      <property role="IQ2nx" value="8157872828699417091" />
      <property role="TrG5h" value="startDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinA9X" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112637" />
      <property role="TrG5h" value="status" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinAa2" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112642" />
      <property role="TrG5h" value="boardNumber" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinAaf" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112655" />
      <property role="TrG5h" value="plannedGas" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinAan" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112663" />
      <property role="TrG5h" value="consignee" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinAaH" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112685" />
      <property role="TrG5h" value="aircraftType" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinAaK" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112688" />
      <property role="TrG5h" value="parkingNumber" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3vJGxNinAaL" role="1TKVEl">
      <property role="IQ2nx" value="4030636028378112689" />
      <property role="TrG5h" value="airlineRepresentative" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="3vJGxNinAbX" role="1TKVEi">
      <property role="IQ2ns" value="4030636028378112765" />
      <property role="20kJfa" value="contract" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSIp" resolve="Contract" />
    </node>
  </node>
  <node concept="1TIwiD" id="3vJGxNinAbW">
    <property role="EcuMT" value="4030636028378112764" />
    <property role="TrG5h" value="WithdrawalSlip" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="3vJGxNinAc1" role="1TKVEi">
      <property role="IQ2ns" value="4030636028378112769" />
      <property role="20kJfa" value="contract" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQdrSIp" resolve="Contract" />
    </node>
    <node concept="1TJgyj" id="7AdOa6oOAMj" role="1TKVEi">
      <property role="IQ2ns" value="8758886242777722003" />
      <property role="20kJfa" value="request" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="74Q_xQds9BM" resolve="Request" />
    </node>
    <node concept="1TJgyj" id="3vJGxNinAc3" role="1TKVEi">
      <property role="IQ2ns" value="4030636028378112771" />
      <property role="20kJfa" value="flight" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="3vJGxNinA9H" resolve="Flight" />
    </node>
    <node concept="1TJgyj" id="3vJGxNinAc6" role="1TKVEi">
      <property role="IQ2ns" value="4030636028378112774" />
      <property role="20kJfa" value="priceChange" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4TC5SpZFOHT" resolve="PriceNotice" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOALZ" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777721983" />
      <property role="TrG5h" value="number" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAM1" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777721985" />
      <property role="TrG5h" value="creationDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAM4" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777721988" />
      <property role="TrG5h" value="negotiation" />
      <ref role="AX2Wp" node="7UJp4hhe3vD" resolve="Negotiation" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAM8" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777721992" />
      <property role="TrG5h" value="agreedDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAMd" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777721997" />
      <property role="TrG5h" value="startDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAMo" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777722008" />
      <property role="TrG5h" value="finishDateTime" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAMv" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777722015" />
      <property role="TrG5h" value="TZAoperator" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAMB" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777722023" />
      <property role="TrG5h" value="controlTalonNumber" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="7AdOa6oOAMK" role="1TKVEi">
      <property role="IQ2ns" value="8758886242777722032" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="gas" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="7AdOa6oOAMQ" resolve="Gas" />
    </node>
  </node>
  <node concept="1TIwiD" id="7AdOa6oOAMQ">
    <property role="TrG5h" value="Gas" />
    <property role="EcuMT" value="8758886242777722038" />
    <node concept="1TJgyi" id="7AdOa6oOAMR" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777722039" />
      <property role="TrG5h" value="name" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOANs" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777722076" />
      <property role="TrG5h" value="IATA" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAMT" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777722041" />
      <property role="TrG5h" value="volume" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAMW" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777722044" />
      <property role="TrG5h" value="mass" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAN0" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777722048" />
      <property role="TrG5h" value="density" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOAN5" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777722053" />
      <property role="TrG5h" value="temperature" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7AdOa6oOANb" role="1TKVEl">
      <property role="IQ2nx" value="8758886242777722059" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
</model>

